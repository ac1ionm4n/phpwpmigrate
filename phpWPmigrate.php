<?php
require 'wp-config.php';
$errors = "";
$sqlToWrite = "";
$myDump = "";
$handle = "";
$configFile = 'wp-config.php';
$dbname = DB_NAME;
$dbuser = DB_USER;
$dbpass = DB_PASSWORD;
$dbhost = DB_HOST; 
$con = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname); 
$dtCon = mysqli_connect($dbhost,$dbuser,$dbpass, 'information_schema');
if (!$con) {
        $errors = $errors . "<p>ERROR: Unable to connect to $dbname.";
        $errors = $errors . "<p>Debugging errno: " . mysqli_connect_errno() . PHP_EOL . "</p>";
        $errors = $errors . "<p>Debugging error: " . mysqli_connect_error() . PHP_EOL . "</p>";
    }
if (empty($errors)){
    echo "<h5>Connection to $dbname successful</h5>";
}
if(!$dtCon) {
        $errors = $errors . "<p>ERROR: Unable to connect to imformation_schema.";
        $errors = $errors . "<p>Debugging errno: " . mysqli_connect_errno() . PHP_EOL . "</p>";
        $errors = $errors . "<p>Debugging error: " . mysqli_connect_error() . PHP_EOL . "</p>";
}
if (empty($errors)){
    echo "<h5>Connection to information_schema successful</h5>";
}
$tables = array();
$dir = getcwd();
if(empty($errors)){
    $myDump = "$dbname-".date("Y-m-d-H-i")."-dump.sql";
    $handle = fopen($myDump, 'w') or die('Cannot open file:  '.$myDump);
    $query = "SHOW tables";
    $result = mysqli_query($con,$query);
    $tableNum = 0;
    while ($row = mysqli_fetch_array($result)) {
        $tables[$tableNum] = $row[0];
        $tableName = $tables[$tableNum];
        $sqlToWrite = $sqlToWrite . "CREATE TABLE $tableName ("."\n";
        //echo "<h4>Table Number $tableNum is $tableName</h4>";
        $primaryKeyQuery = "SHOW KEYS FROM $tableName WHERE Key_name = 'PRIMARY'";
        $getPrimaryKey = mysqli_query($con,$primaryKeyQuery);
        $primaryKey = "";
        while($pKey = mysqli_fetch_array($getPrimaryKey)){
            $primaryKey = $pKey['Column_name'];
        }
        //echo "<p>The Primary Key is $primaryKey";
        $tableQuery = "SELECT * FROM $tableName";
        $getTableData = mysqli_query($con,$tableQuery);
        //$numTableRows = mysqli_num_rows($getTableData);
        $numTableFields = mysqli_num_fields($getTableData);
        //echo "<p>$tableName has $numTableRows rows and $numTableFields fields</p>";
        $fieldName = array();
        //echo "<table border=1 ><tr>";
        for ($fieldNum = 0; $fieldNum < $numTableFields; $fieldNum++){
            $field =  mysqli_fetch_field_direct ($getTableData,$fieldNum);
            $fieldName[$fieldNum] = $field->name;
            $datatype ="";
            $fName = $fieldName[$fieldNum];
            $getDataType = "SELECT * FROM COLUMNS WHERE TABLE_NAME LIKE '$tableName' AND COLUMN_NAME LIKE '$fName'";
            $returnDataType = mysqli_query($dtCon,$getDataType);
            while($dt = mysqli_fetch_array($returnDataType)){
                $datatype = $dt['DATA_TYPE'];
                if ($datatype = "longtext"){
                    $datatype = "text";
                }
            }
            //echo "<th>$fieldName[$fieldNum] - $datatype</th>";
            if ($fieldName == $primaryKey) {
                $sqlToWrite = $sqlToWrite . "$fieldName[$fieldNum] $datatype PRIMARY KEY,"."\n";
            }
            else if($fieldNum == ($numTableFields-1)){                
                $sqlToWrite = $sqlToWrite . "$fieldName[$fieldNum] $datatype"."\n";                
            }
            else {
                $sqlToWrite = $sqlToWrite . "$fieldName[$fieldNum] $datatype,"."\n";
            }
            
            //fwrite($handle, $sqlToWrite);
        }
        $sqlToWrite = $sqlToWrite . ");"."\n\n";
        //fwrite($handle, $sqlToWrite);
        //echo "</tr>";
        while($tRow = mysqli_fetch_array($getTableData)){
            //echo "<tr>";
            $sqlToWrite = $sqlToWrite . "INSERT INTO $tableName("."\n";
            //fwrite($handle, $sqlToWrite);
            for ($fieldNum = 0; $fieldNum < $numTableFields; $fieldNum++){
                //echo "<td>$tRow[$fieldNum]</td>";
                if ($fieldNum == ($numTableFields-1)){
                    $sqlToWrite = $sqlToWrite . "$fieldName[$fieldNum])"."\n";
                }
                else {
                    $sqlToWrite = $sqlToWrite . "$fieldName[$fieldNum],"."\n";
                }
                //fwrite($handle, $sqlToWrite);
            }
            $sqlToWrite = $sqlToWrite . "VALUES("."\n";
            //fwrite($handle, $sqlToWrite);
            for ($fieldNum = 0; $fieldNum < $numTableFields; $fieldNum++){
                if ($fieldNum == ($numTableFields-1)){
                    $sqlToWrite = $sqlToWrite . "$tRow[$fieldNum]);"."\n\n";
                }
                else {
                    $sqlToWrite = $sqlToWrite . "$tRow[$fieldNum],"."\n";
                }
                //fwrite($handle, $sqlToWrite);
            }
            //echo "</tr>";
        }  
        //echo "</table>";
        $tableNum++;
    }
    fwrite($handle, $sqlToWrite);
    echo "<h4>SQL Dump Created</h4>";
    $path = realpath(__DIR__);
    $zip = new ZipArchive();
    $zip->open('phpWpMigration.zip', ZipArchive::CREATE | ZipArchive::OVERWRITE);
    $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path));
    foreach ($files as $name => $file)
    {
        if ($file->isDir()) {
            flush();
            continue;
        }
        
        $filePath = $file->getRealPath();$_SERVER['REQUEST_URI']."/phpWpMigration.zip";
        $relativePath = substr($filePath, strlen($path) + 1);
        
        $zip->addFile($filePath, $relativePath);
    }
    $zip->close();

    echo "<h4>Archive Created</h4>";
    print_r(apache_request_headers());
    $currentDomain = preg_replace('/www\./i', '', $_SERVER['SERVER_NAME']);
    $downloadUrl = $currentDomain."/phpWpMigration.zip";
    echo "<p>Download Here: <a href='phpWpMigration.zip'>$downloadUrl</a></p>";
} else {

    echo $errors;

}

?>