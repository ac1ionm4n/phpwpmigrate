# README #

PLEASE NOTE:  This is an incomplete work in progress.

### What is this repository for? ###

* Create an SQL dump and .zip archive of your WordPress website by running a single PHP document, currently only works for smaller websites.
* Version 0.0.1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* To set up, simply upload phpWPmigrate.php to the same directory as your wp-config.php file.
* Set your PHP Memory limit as high as you can with your hosting configuration.
* Simply run the document in your web browser and an SQL dump file as well as a zipped archive of your sites files will be created.  (may take a few minutes)

### Who do I talk to? ###

* adam.langley@live.com